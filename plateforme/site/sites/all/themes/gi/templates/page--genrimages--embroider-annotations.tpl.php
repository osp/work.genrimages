<?php
$gi_prefix='';
if ($_SERVER['SERVER_NAME'] === 'localhost') {
  $gi_prefix='/genrimages';
}
?>
<div id="gi_page">
   <div id="menu_editors">
<span class="menu_editor_item"><a href="<?php print $gi_prefix;?>/video-frame/video-frame.html">Importer une image (video)</a></span> &middot; 
<span class="menu_editor_item"><?php print l(t('Créer un document visuel'),'node/add/document-visuel',array('attributes' => array('class' => 'menu_link')));?></span> &middot; 
<span class="menu_editor_item"><?php print l(t('Voir les documents visuels'),'genrimages/documents-visuels',array('attributes' => array('class' => 'menu_link')));?></span> &middot; 
<span class="menu_editor_item"><?php print l(t('Créer une fiche'),'genrimages/annotation_ensemble_edit',array('attributes' => array('class' => 'menu_link')));?></span> &middot; 
<span class="menu_editor_item"><?php print l(t('Voir les fiches'),'genrimages/fiches',array('attributes' => array('class' => 'menu_link')));?></span>&middot; 
<span class="menu_editor_item"><?php print l(t('Voir toutes les annotations'),'genrimages/voir-les-annotations',array('attributes' => array('class' => 'menu_link')));?></span>
</div>
		<div id="panel_left">
   <?php print_r($page['content']['system_main']['#markup']); ?>		
		</div>
		<div id="panel_right_env" style="padding-left:130px">
		<!---<div id="save_all" class="gi_button">Enregistrer</div>-->
	        <div id="to_explain" class="gi_button"><a href="" class="button_a">Voir notes</a></div>
		<div id="to_question" class="gi_button"><a href="" class="button_a">Voir questions</a></div>
		<div class="clear"></div>
                <div class="layer_header">notes</div>
		<div id="panel_right"></div></div>
<br class="clear" />
</div>
<br class="clear" />
<input type="hidden" id="id_tmp_note" value="" />
   <script>
   jQuery(document).ready(function($) {
       var cookie_config='notes-<?php print $user->uid;?>-'+$('#liste_notes_id').val();
       console.log(cookie_config);
       if(Cookies.get(cookie_config)){
	 var config_space=Cookies.getJSON(cookie_config);
	 $('#panel_right_env').offset({top:config_space.top,left:config_space.left});
       }else{
         var config_space={};
}

<?php
  if ($_SERVER['SERVER_NAME'] === 'localhost') {
    print "var get_url='/genrimages/plateforme/site/?q=genrimages/get_js_annotations/'+$('#liste_notes_id').val();\n";
    print "var save_url='/genrimages/plateforme/site/?q=genrimages/create_annotation_fragment';\n";
    print "var delete_url='/genrimages/plateforme/site/?q=genrimages/delete_annotation_fragment';\n";
  }else{
    print "var get_url='/plateforme/site/?q=genrimages/get_js_annotations/'+$('#liste_notes_id').val();";
    print "var save_url='/plateforme/site/?q=genrimages/create_annotation_fragment';\n";
    print "var delete_url='/plateforme/site/?q=genrimages/delete_annotation_fragment';\n";
}
?>
    $( "#panel_right_env" ).draggable({ 
      handle: ".layer_header",
      stop: function() {
	  var offset=$( "#panel_right_env" ).offset();
	  config_space.top=offset.top;
	  config_space.left=offset.left;
	  console.log('Writing '+cookie_config+' value '+config_space);
	  Cookies.set(cookie_config,config_space);
      } 
	  });
       var mode=$('#mode').val();
       if(mode=="notes"){
	 $('#to_question').hide();
       }else{
	 $('#to_explain').hide();
	 $('.layer_header').html('questions');
       }
       $("#toAnnotate").annotateImage({
	 getUrl: get_url,
	     saveUrl: save_url,
	     deleteUrl: delete_url,
	     editable: true
	     });
     });
</script>