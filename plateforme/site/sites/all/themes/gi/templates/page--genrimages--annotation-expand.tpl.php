<?php
$gi_prefix='';
if ($_SERVER['SERVER_NAME'] === 'localhost') {
  $gi_prefix='/genrimages';
}
?>
<div id="gi_page">
   <div id="menu_editors">
<span class="menu_editor_item"><a href="<?php print $gi_prefix;?>/video-frame/video-frame.html">Importer une image (video)</a></span> &middot; 
<span class="menu_editor_item"><?php print l(t('Créer un document visuel'),'node/add/document-visuel',array('attributes' => array('class' => 'menu_link')));?></span> &middot; 
<span class="menu_editor_item"><?php print l(t('Voir les documents visuels'),'genrimages/documents-visuels',array('attributes' => array('class' => 'menu_link')));?></span> &middot; 
<span class="menu_editor_item"><?php print l(t('Créer une fiche'),'genrimages/annotation_ensemble_edit',array('attributes' => array('class' => 'menu_link')));?></span> &middot; 
<span class="menu_editor_item"><?php print l(t('Voir les fiches'),'genrimages/fiches',array('attributes' => array('class' => 'menu_link')));?></span>&middot; 
<span class="menu_editor_item"><?php print l(t('Voir toutes les annotations'),'genrimages/voir-les-annotations',array('attributes' => array('class' => 'menu_link')));?></span>
</div>
<?php
print render($page['content']);
?>
<br class="clear" />
</div>
<br class="clear" />
<div id="edit_liens"></div>