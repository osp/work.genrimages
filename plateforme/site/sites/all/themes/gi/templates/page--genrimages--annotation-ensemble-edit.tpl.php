<?php
$gi_prefix='';
if ($_SERVER['SERVER_NAME'] === 'localhost') {
  $gi_prefix='/genrimages';
}
?>
<div id="gi_page">
  <div id="menu_editors">
<span class="menu_editor_item"><a href="<?php print $gi_prefix;?>/video-frame/video-frame.html">Importer une image (video)</a></span> &middot; 
<span class="menu_editor_item"><a href="<?php print $gi_prefix;?>/plateforme/site/?q=node/add/document-visuel" class="menu_link">Créer un document visuel</a></span> &middot; 
<span class="menu_editor_item"><a href="<?php print $gi_prefix;?>/plateforme/site/?q=genrimages/documents-visuels" class="menu_link">Voir les documents visuels</a></span> &middot; 
<span class="menu_editor_item"><a href="<?php print $gi_prefix;?>/plateforme/site/?q=genrimages/annotation_ensemble_edit" class="menu_link">Créer une fiche</a></span> &middot; 
<span class="menu_editor_item"><a href="<?php print $gi_prefix;?>/plateforme/site/?q=genrimages/fiches" class="menu_link">Voir les fiches</a></span>&middot; 
<span class="menu_editor_item"><a href="<?php print $gi_prefix;?>/plateforme/site/?q=genrimages/voir-les-annotations" class="menu_link">Voir toutes les annotations</a></span>
</div>
<div id="panel_left">
<?php
print render($page['content']);
?>
</div>
<div id="panel_right_env">
</div>
<br class="clear" />
</div>
<br class="clear" />
<div id="edit_liens"></div>