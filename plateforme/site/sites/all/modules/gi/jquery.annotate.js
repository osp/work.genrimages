/// <reference path="jquery-1.2.6-vsdoc.js" />
(function($) {

    $.fn.annotateImage = function(options) {
        ///	<summary>
        ///		Creates annotations on the given image.
        ///     Images are loaded from the "getUrl" propety passed into the options.
        ///	</summary>
	//console.log("loading");
        var opts = $.extend({}, $.fn.annotateImage.defaults, options);
        var image = this;

        this.image = this;
        this.mode = 'view';
	this.ref="Polo";
	this.gibag=[];
        // Assign defaults
        this.getUrl = opts.getUrl;
        this.saveUrl = opts.saveUrl;
        this.deleteUrl = opts.deleteUrl;
        this.editable = opts.editable;
        this.useAjax = opts.useAjax;
        this.notes = opts.notes;
        this.orderUrl = opts.orderUrl;

        // Add the canvas
        this.canvas = $('<div class="image-annotate-canvas"><div class="image-annotate-view"></div><div class="image-annotate-edit"><div class="image-annotate-edit-area"></div></div></div>');
        this.canvas.children('.image-annotate-edit').hide();
        this.canvas.children('.image-annotate-view').hide();
        this.image.after(this.canvas);

        // Give the canvas and the container their size and background
        this.canvas.height(this.height());
        this.canvas.width(this.width());
        this.canvas.css('background-image', 'url("' + this.attr('src') + '")');
        this.canvas.children('.image-annotate-view, .image-annotate-edit').height(this.height());
        this.canvas.children('.image-annotate-view, .image-annotate-edit').width(this.width());

        // Add the behavior: hide/show the notes when hovering the picture
        this.canvas.hover(function() {
            if ($(this).children('.image-annotate-edit').css('display') == 'none') {
                $(this).children('.image-annotate-view').show();
		$('.image-annotate-area').show();
            }
        }, function() {
            $(this).children('.image-annotate-view').hide();
	    $('.image-annotate-note').hide();
        });

        this.canvas.children('.image-annotate-view').hover(function() {
            $(this).show();
        }, function() {
            $(this).hide();
        });

        // load the notes
        if (this.useAjax) {
            $.fn.annotateImage.ajaxLoad(this);
        } else {
            $.fn.annotateImage.load(this);
        }

        // Add the "Add a note" button
        if (this.editable) {
	    var addlabel='Ajouter une note';
	    if($('#mode').val()=='questions'){
		addlabel='Ajouter une question';
	    }
            this.button = $('<a class="image-annotate-add" id="image-annotate-add" href="#">'+addlabel+'</a>');
            this.button.click(function() {
                $.fn.annotateImage.add(image);
            });
            //this.canvas.after(this.button);
	    $("#panel_right_env").prepend(this.button);
        }

        // Hide the original
        this.hide();

        return this;
    };

    /**
    * Plugin Defaults
    **/
    $.fn.annotateImage.defaults = {
        getUrl: '?q=genrimages/get_js_annotations/',
        saveUrl: '?q=genrimages/create_annotation_fragment',
        deleteUrl: '?q=genrimages/delete_annotation_fragment',
        orderUrl: '?q=genrimages/order_annotation_fragments',
        editable: true,
        useAjax: true,
        notes: new Array()
    };

    $.fn.annotateImage.layers =  function(image) {
	//console.log("entering layers");
	image.canvas.children('.image-annotate-view').show();
        for (var i = 0; i < image.notes.length; i++) {
            //console.log(image.notes[i]);
	    var html_lay='<div class="layer" id="control_lay_'+i+'" data-id="'+i+'">'+image.notes[i].note.text+' <a href="?q=genrimages/annotation_expand/'+image.notes[i].note.id+'">+</a></div>';
	    $('#panel_right').append(html_lay);
	    //console.log(image.notes[image.notes[i]]);
	    //var inote=image.notes[image.notes[i]];
	    //inote.show();
	    (function(i) {
		var id_ctrl='#control_lay_'+i;
		$(id_ctrl).click(function(){
		    (function(i) {
			$(".image-annotate-area").each(function(){
			    if($(this).attr("data-note")==image.notes[i].note.id){
				$(this).show();
				//console.log($(this).attr("data-note"));
				//console.log(image.notes[i].note.id);
			    }else{
				$(this).hide();
			    }
			});
		    }(i));

		    for (var j = 0; j < image.notes.length; j++) {
			if(j==i){ image.notes[i].show();
				}else{
				    image.notes[j].hide();
				    //console.log(image.notes[j]);
				}	
		    }
		    //image.notes[i].show();
		    image.canvas.children('.image-annotate-view').show();
		    //alert(i);
		});
	    }(i));
        }
	$('#panel_right').sortable({update: function( event, ui ) {
	    $.fn.annotateImage.saveOrder(image);
}});
	$("#save_all").click(function(){
	   //console.log("firing saveAll");
	    $.fn.annotateImage.saveAsJson(image);
	});	
    };


    $.fn.annotateImage.clear = function(image) {
        ///	<summary>
        ///		Clears all existing annotations from the image.
        ///	</summary>    
        for (var i = 0; i < image.notes.length; i++) {
            image.notes[image.notes[i]].destroy();
        }
        image.notes = new Array();
    };

    $.fn.annotateImage.ajaxLoad = function(image) {
        ///	<summary>
        ///		Loads the annotations from the "getUrl" property passed in on the
        ///     options object.
        ///	</summary>
	//console.log(image.getUrl);
        $.getJSON(image.getUrl +'&ticks='+ $.fn.annotateImage.getTicks(), function(data) {
	    //console.log(data);
            image.notes = data;
            $.fn.annotateImage.load(image);
        });
    };

    $.fn.annotateImage.load = function(image) {
	//console.log("entering load");
        ///	<summary>
        ///		Loads the annotations from the notes property passed in on the
        ///     options object.
        ///	</summary>
        for (var i = 0; i < image.notes.length; i++) {
            image.notes[i] = new $.fn.annotateView(image, image.notes[i]);
	    //gibag.push();
        }
	$.fn.annotateImage.layers(image);
    };

    $.fn.annotateImage.getTicks = function() {
        ///	<summary>
        ///		Gets a count og the ticks for the current date.
        ///     This is used to ensure that URLs are always unique and not cached by the browser.
        ///	</summary>        
        var now = new Date();
        return now.getTime();
    };

    $.fn.annotateImage.add = function(image) {
        ///	<summary>
        ///		Adds a note to the image.
        ///	</summary>        
        if (image.mode == 'view') {
            image.mode = 'edit';

            // Create/prepare the editable note elements
            var editable = new $.fn.annotateEdit(image);

            $.fn.annotateImage.createSaveButton(editable, image);
            $.fn.annotateImage.createCancelButton(editable, image);
        }
    };

    $.fn.annotateImage.createSaveButton = function(editable, image, note) {
        ///	<summary>
        ///		Creates a Save button on the editable note.
        ///	</summary>
	//console.log("Editable: "+editable);
        var ok = $('<a class="image-annotate-edit-ok">OK</a>');
        ok.click(function() {
            var form = $('#image-annotate-edit-form form');
            var text = $('#image-annotate-text').val();
            $.fn.annotateImage.appendPosition(form, editable)
            image.mode = 'view';

            // Save via AJAX
            if (image.useAjax) {
		//console.log(form.serialize());
		//console.log(JSON.stringify(form));
		//console.log(image.saveUrl);
		//object to save (ots)
		var ots={};
		ots.height=$("input[name='height']").val();
		ots.width=$("input[name='width']").val();
		ots.top=$("input[name='top']").val();
		ots.left=$("input[name='left']").val();
		ots.id=$("input[name='id']").val();
		ots.liste_notes=$('#liste_notes_id').val();
		ots.text=text;
		var ots_json=JSON.stringify(ots);
                $.ajax({
                    type: "POST",
                    url: image.saveUrl,
                    //data: form.serialize(),
		    data: {config: ots_json},
                    error: function(x,s,e) { 
			console.log(s.responseText) 
		    },
                    success: function(data) {
			console.log("From success function");
			console.log(data);
				if (data.id != undefined) {
					editable.note.id = data.id;
				    console.log('ed '+editable.note.id);
				    $('#id_tmp_note').val(data.id);
				    $('.ltmp').attr('href','?q=genrimages/annotation_expand/'+data.id);
				    $('.ltmp').html('+');
				    $('.ltmp').removeClass('ltmp');


				}
		    },
                    dataType: "json"
                });
            }

            // Add to canvas
            if (note) {
                note.resetPosition(editable, text);
		//console.log("Node id: "+note.note.id);
		console.log(note.note.id);
		var ltgt;
		for(var j=0;j<image.notes.length;j++){
		    console.log(image.notes[j]);
		    if(image.notes[j].note.id==note.note.id){
			ltgt=j;
			break
		    }
		}
		var lid='#control_lay_'+ltgt;
		console.log(lid);
		$(lid).html(note.note.text);
            } else {
                editable.note.editable = true;
                note = new $.fn.annotateView(image, editable.note)
                note.resetPosition(editable, text);
                //image.notes.push(editable.note);
                image.notes.push(note);
		var i_ctrl=(image.notes.length-1);
		var id_ctrl='control_lay_'+i_ctrl;
		var jq_ctrl='#'+id_ctrl;
		console.log(jq_ctrl);
		var html_lay='<div class="layer" id="'+id_ctrl+'"  data-id="'+i_ctrl+'">'+text;
		html_lay+= ' <a class="ltmp" href="">-</a></div>';
		//console.log("SNIP"+snip_link);
		
		$('#panel_right').append(html_lay);
		$('#panel_right').sortable({update: function( event, ui ) {
	    $.fn.annotateImage.saveOrder(image);
}});
		$(jq_ctrl).click(function(){
		    //alert("click");
		    for (var j = 0; j < image.notes.length; j++) {
			if(j==i_ctrl){ image.notes[j].show();
				     }else{
			    image.notes[j].hide();
			    //console.log(image.notes[j]);
			}	
		    }
		    //image.notes[i].show();
		    image.canvas.children('.image-annotate-view').show();
		    //alert(i);
		});
            }

            editable.destroy();
        });
        editable.form.append(ok);
    };

    $.fn.annotateImage.createCancelButton = function(editable, image) {
        ///	<summary>
        ///		Creates a Cancel button on the editable note.
        ///	</summary>
        var cancel = $('<a class="image-annotate-edit-close">Cancel</a>');
        cancel.click(function() {
            editable.destroy();
            image.mode = 'view';
        });
        editable.form.append(cancel);
    };

    $.fn.annotateImage.saveAsHtml = function(image, target) {
        var element = $(target);
        var html = "";
        for (var i = 0; i < image.notes.length; i++) {
            html += $.fn.annotateImage.createHiddenField("text_" + i, image.notes[i].text);
            html += $.fn.annotateImage.createHiddenField("top_" + i, image.notes[i].top);
            html += $.fn.annotateImage.createHiddenField("left_" + i, image.notes[i].left);
            html += $.fn.annotateImage.createHiddenField("height_" + i, image.notes[i].height);
            html += $.fn.annotateImage.createHiddenField("width_" + i, image.notes[i].width);
        }
        element.html(html);
    };

    $.fn.annotateImage.saveAsJson = function(image) {
	var obj_save;
	var ar_save_tmp=[];
	var ar_save=[];
        var str = "";
        for (var i = 0; i < image.notes.length; i++) {
            str += "name_" +i+" : "+$("#toAnnotate").attr("data-name");
            str += "text_" +i+" : "+image.notes[i].note.text;
            str +=  "top_" +i+" : "+image.notes[i].note.top;
            str +=  "left_" +i+" : "+ image.notes[i].note.left;
            str +=  "height_" +i+" : "+image.notes[i].note.height;
            str +=  "width_" +i+" : "+image.notes[i].note.width;
	    str+="\n";
	    ar_save_tmp.push(image.notes[i].note);
	    console.log(image.notes[i].note);
        }
	var sort_result = $("#panel_right").sortable('toArray', {attribute: 'data-id'});
	//console.log();
	for(var j=0;j<sort_result.length;j++){
	    ar_save.push(ar_save_tmp[sort_result[j]]);
	}
        //console.log(JSON.stringify(ar_save));

        $.ajax({
            type: "POST",
            url: image.saveUrl,
            data: {config: JSON.stringify(ar_save)},
            error: function(x,s,e) { 
		//console.log(s.responseText) 
	    },
            success: function(data) {
		//console.log("saved correctly");
	    },
            dataType: "json"
        });

    };

    $.fn.annotateImage.saveOrder = function(image) {
	console.log("Entering saveOrder");
	var obj_save={};
	var ar_save_tmp=[];
	var ar_save=[];
        var str = "";
        for (var i = 0; i < image.notes.length; i++) {
	    ar_save_tmp.push(image.notes[i].note);
	    console.log(image.notes[i].note);
        }
	var sort_result = $("#panel_right").sortable('toArray', {attribute: 'data-id'});
	//console.log();
	for(var j=0;j<sort_result.length;j++){
	    ar_save.push(ar_save_tmp[sort_result[j]].id);
	}
        //console.log(JSON.stringify(ar_save));
	//obj_save.order=sort_result;
	obj_save.order=ar_save;
	obj_save.liste_notes=$('#liste_notes_id').val();
        $.ajax({
            type: "POST",
            url: image.orderUrl,
            data: {config: JSON.stringify(obj_save)},
            error: function(x,s,e) { 
		//console.log(s.responseText) 
	    },
            success: function(data) {
		//console.log("saved correctly");
	    },
            dataType: "json"
        });

    };


    $.fn.annotateImage.createHiddenField = function(name, value) {
        return '&lt;input type="hidden" name="' + name + '" value="' + value + '" /&gt;<br />';
    };

    $.fn.annotateEdit = function(image, note) {
        ///	<summary>
        ///		Defines an editable annotation area.
        ///	</summary>
        this.image = image;

        if (note) {
            this.note = note;
        } else {
            var newNote = new Object();
            newNote.id = "gi_"+(new Date()).getTime();;
            newNote.top = 30;
            newNote.left = 30;
            newNote.width = 30;
            newNote.height = 30;
            newNote.text = "";
	    newNote.fname = $("#toAnnotate").attr("data-name");
            this.note = newNote;
        }

        // Set area
        var area = image.canvas.children('.image-annotate-edit').children('.image-annotate-edit-area');
        this.area = area;
        this.area.css('height', this.note.height + 'px');
        this.area.css('width', this.note.width + 'px');
        this.area.css('left', this.note.left + 'px');
        this.area.css('top', this.note.top + 'px');

        // Show the edition canvas and hide the view canvas
        image.canvas.children('.image-annotate-view').hide();
        image.canvas.children('.image-annotate-edit').show();

        // Add the note (which we'll load with the form afterwards)
        var form = $('<div id="image-annotate-edit-form"><form><textarea id="image-annotate-text" name="text" rows="3" cols="30">' + this.note.text + '</textarea></form></div>');
        this.form = form;

        $('body').append(this.form);
        this.form.css('left', this.area.offset().left + 'px');
        this.form.css('top', (parseInt(this.area.offset().top) + parseInt(this.area.height()) + 7) + 'px');

        // Set the area as a draggable/resizable element contained in the image canvas.
        // Would be better to use the containment option for resizable but buggy
        area.resizable({
            handles: 'all',

            stop: function(e, ui) {
                form.css('left', area.offset().left + 'px');
                form.css('top', (parseInt(area.offset().top) + parseInt(area.height()) + 2) + 'px');
            }
        })
        .draggable({
            containment: image.canvas,
            drag: function(e, ui) {
                form.css('left', area.offset().left + 'px');
                form.css('top', (parseInt(area.offset().top) + parseInt(area.height()) + 2) + 'px');
            },
            stop: function(e, ui) {
                form.css('left', area.offset().left + 'px');
                form.css('top', (parseInt(area.offset().top) + parseInt(area.height()) + 2) + 'px');
            }
        });
        return this;
    };

    $.fn.annotateEdit.prototype.destroy = function() {
        ///	<summary>
        ///		Destroys an editable annotation area.
        ///	</summary>        
        this.image.canvas.children('.image-annotate-edit').hide();
        this.area.resizable('destroy');
        this.area.draggable('destroy');
        this.area.css('height', '');
        this.area.css('width', '');
        this.area.css('left', '');
        this.area.css('top', '');
        this.form.remove();
    }

    $.fn.annotateView = function(image, note) {
        ///	<summary>
        ///		Defines a annotation area.
        ///	</summary>
        this.image = image;

        this.note = note;

        this.editable = (note.editable && image.editable);

        // Add the area
	var areaNoteId=this.note.id;
        this.area = $('<div class="image-annotate-area' + (this.editable ? ' image-annotate-area-editable' : '') + '" data-note="'+areaNoteId+'"><div></div></div>');
        image.canvas.children('.image-annotate-view').prepend(this.area);

        // Add the note
        this.form = $('<div class="image-annotate-note">' + note.text + '</div>');
        this.form.hide();
        image.canvas.children('.image-annotate-view').append(this.form);
        this.form.children('span.actions').hide();

        // Set the position and size of the note
        this.setPosition();

        // Add the behavior: hide/display the note when hovering the area
        var annotation = this;
        this.area.hover(function() {
            annotation.show();
        }, function() {
            annotation.hide();
        });

        // Edit a note feature
        if (this.editable) {
            var form = this;
            this.area.click(function() {
                form.edit();
            });
        }
    };

    $.fn.annotateView.prototype.setPosition = function() {
        ///	<summary>
        ///		Sets the position of an annotation.
        ///	</summary>
        this.area.children('div').height((parseInt(this.note.height) - 2) + 'px');
        this.area.children('div').width((parseInt(this.note.width) - 2) + 'px');
        this.area.css('left', (this.note.left) + 'px');
        this.area.css('top', (this.note.top) + 'px');
        this.form.css('left', (this.note.left) + 'px');
        this.form.css('top', (parseInt(this.note.top) + parseInt(this.note.height) + 7) + 'px');
    };

    $.fn.annotateView.prototype.show = function() {
	console.log("receiving call from show")
	console.log(this.note.text)
        ///	<summary>
        ///		Highlights the annotation
        ///	</summary>
        this.form.fadeIn(250);
        if (!this.editable) {
            this.area.addClass('image-annotate-area-hover');
        } else {
            this.area.addClass('image-annotate-area-editable-hover');
        }
    };

    $.fn.annotateView.prototype.hide = function() {
        ///	<summary>
        ///		Removes the highlight from the annotation.
        ///	</summary>      
        this.form.fadeOut(250);
        this.area.removeClass('image-annotate-area-hover');
        this.area.removeClass('image-annotate-area-editable-hover');
    };

    $.fn.annotateView.prototype.destroy = function() {
        ///	<summary>
        ///		Destroys the annotation.
        ///	</summary>      
        this.area.remove();
        this.form.remove();
    }

    $.fn.annotateView.prototype.edit = function() {
        ///	<summary>
        ///		Edits the annotation.
        ///	</summary>      
        if (this.image.mode == 'view') {
            this.image.mode = 'edit';
            var annotation = this;

            // Create/prepare the editable note elements
            var editable = new $.fn.annotateEdit(this.image, this.note);

            $.fn.annotateImage.createSaveButton(editable, this.image, annotation);
	    var that_note=this.note;
            // Add the delete button
            var del = $('<a class="image-annotate-edit-delete">Delete</a>');
            del.click(function() {
                var form = $('#image-annotate-edit-form form');
		//console.log("Attempting to delete node "+that_note.id);
		var that_url=annotation.image.deleteUrl+'/'+that_note.id;
                $.fn.annotateImage.appendPosition(form, editable)
		var checkdel=confirm("Vous voulez effacer cette note?");
		if(checkdel){
		    window.location.href = that_url;
                    annotation.image.mode = 'view';
                    editable.destroy();
                    annotation.destroy();
		}else{
		    //window.location.reload();
		}

            });
            editable.form.append(del);
            $.fn.annotateImage.createCancelButton(editable, this.image);
        }
    };

    $.fn.annotateImage.appendPosition = function(form, editable) {
        ///	<summary>
        ///		Appends the annotations coordinates to the given form that is posted to the server.
        ///	</summary>
        var areaFields = $('<input type="hidden" value="' + editable.area.height() + '" name="height"/>' +
                           '<input type="hidden" value="' + editable.area.width() + '" name="width"/>' +
                           '<input type="hidden" value="' + editable.area.position().top + '" name="top"/>' +
                           '<input type="hidden" value="' + editable.area.position().left + '" name="left"/>' +
                           '<input type="hidden" value="' + editable.note.id + '" name="id"/>');
        form.append(areaFields);
    }

    $.fn.annotateView.prototype.resetPosition = function(editable, text) {
        ///	<summary>
        ///		Sets the position of an annotation.
        ///	</summary>
        this.form.html(text);
        this.form.hide();

        // Resize
        this.area.children('div').height(editable.area.height() + 'px');
        this.area.children('div').width((editable.area.width() - 2) + 'px');
        this.area.css('left', (editable.area.position().left) + 'px');
        this.area.css('top', (editable.area.position().top) + 'px');
        this.form.css('left', (editable.area.position().left) + 'px');
        this.form.css('top', (parseInt(editable.area.position().top) + parseInt(editable.area.height()) + 7) + 'px');

        // Save new position to note
        this.note.top = editable.area.position().top;
        this.note.left = editable.area.position().left;
        this.note.height = editable.area.height();
        this.note.width = editable.area.width();
        this.note.text = text;
        this.note.id = editable.note.id;
        this.editable = true;
    };

})(jQuery);
