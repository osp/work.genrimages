<?php
function form_classe($form, &$form_state) {
  $hidden_val=0;
  $nom="";
  $fdescription="";
  $test=arg(2);

  if(is_numeric($test)){
    /* dpm(arg(2)); */
    $id=(int) arg(2);
    $n=node_load($id);
    /* dpm($n); */
    $nom=$n->title;
    $fdescription=$n->field_description['und'][0]['safe_value'];
    $hidden_val=$n->nid;
  }

  /* print_r($hidden_val); */
  $form['description'] = array(
			       '#type' => 'item',
			       '#title' => t('Informations'),
			       );
  // This is the first form element. It's a textfield with a label, "Name"
  $form['nom'] = array(
		       '#type' => 'textfield',
		       '#default_value'=>$nom,
		       '#title' => t('Nom'),
		       );
  $form['fdescription'] = array(
			  '#type' => 'textfield',
			  '#default_value'=>$fdescription,
			  '#title' => t('Description'),
			  );

  $form['hidden_id'] = array(
			     '#type' => 'hidden',
			     '#default_value' => $hidden_val,
			     );
  $form['enregistrer'] = array(
			       '#type' => 'submit',
			       '#value' => t('Enregistrer'),
			       );
  return $form;
}

function form_classe_submit($form, &$form_state){
  if($form_state['values']['hidden_id']==0){
    /* dpm($form_state['values']); */
    $nid=form_classe_create_node($form_state);
  }else{
    $nid=form_classe_edit_node($form_state);
}
  /* drupal_set_message("Form submitted"); */
  $form_state['redirect'] = 'genrimages/classe/'.$nid;
}

function form_classe_create_node($form_state){
  global $user;
  $node = new stdClass(); // We create a new node object
  $node->type = "classe"; // Or any other content type you want
  node_object_prepare($node); // Set some default values.
  $node->title = $form_state['values']['nom'];
  /* $node->language = "und"; // Or any language code if Locale module is enabled. More on this below  */
  $node->field_description['und'][0]['value']= $form_state['values']['description'];
  $node->uid = $user->uid;
  // Entity reference field
  $node = node_submit($node); // Prepare node for a submit
  node_save($node); // After this call we'll get a nid 
  /* dpm($node); */
  return $node->nid;
}

function form_classe_edit_node(&$form_state){
  global $user;
  $node = node_load($form_state['values']['hidden_id']);
  /* dpm($form_state['values']['name']); */
  $node->title = $form_state['values']['nom'];
  /* $node->language = "und"; // Or any language code if Locale module is enabled. More on this below  */
  $node->field_description['und'][0]['value']= $form_state['values']['description'];
  $node->uid = $user->uid;
 /* dpm($node); */
  node_save($node);
  return $node->nid;
}


?>