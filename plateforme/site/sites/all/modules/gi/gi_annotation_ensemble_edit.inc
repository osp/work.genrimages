<?php
function form_annotation_ensemble_edit($form, &$form_state) {
  $hidden_val=0;
  $nom="";
  $analyse="";
  $methode='';
  $prolongements='';
  $visuels='';
  $str_visuels='';
  $mots_cles='';
  $niveaux='';
  $mediums='';
  $prolongements_fiches='';
  $prolongements_visuels="";
  $prolongements_ressources='';

  $selectors=array('edit-analyse','edit-methode', 'edit-prolongements');
  $test=arg(2);

  if(is_numeric($test)){
    /* dpm(arg(2)); */
    $id=(int) arg(2);
    $n=node_load($id);
    /* dpm($n); */
    /* Tester si values pour ces champs */
    if(isset($n->title)){
      $nom=$n->title;
    }else{
      $nom="";
    }
    if(isset($n->field_analyse['und'][0]['value'])){
      $analyse=$n->field_analyse['und'][0]['value'];
    }else{
      $analyse="";
    }
    if(isset($n->field_methode['und'][0]['value'])){
      $methode=$n->field_methode['und'][0]['value'];
    }else{
      $methode="";
    }
    if(isset($n->field_prolongements['und'][0]['value'])){
      $prolongements=$n->field_prolongements['und'][0]['value'];
    }else{
      $prolongements="";
    }
    if(isset($n->field_mots_cles['und'][0]['safe_value'])){
      $mots_cles=$n->field_mots_cles['und'][0]['safe_value'];
    }else{
      $mots_cles="";
    }
    if(isset($n->field_niveaux['und'][0]['safe_value'])){
      $niveaux=$n->field_niveaux['und'][0]['safe_value'];
    }else{
      $niveaux="";
    }
    if(isset($n->field_mediums['und'][0]['safe_value'])){
      $mediums=$n->field_mediums['und'][0]['safe_value'];
    }else{
      $mediums="";
    }
    /* $mots_cles=$n->mots_cles['und']; */
    /* $niveaux=$n->field_niveaux['und']; */
    /* $mediums=$n->field_mediums['und']; */

    $listes=get_liste_notes($id);
    $str_listes='';
    if(count($listes)>0){
      /* print_r($listes); */
      $str_listes='<hr /><h2>Notes et questionnaires liés à cette fiche</h2>';
      foreach($listes as $ar_infos){
	$str_title=$ar_infos['title'].' (<i>'.$ar_infos['type'].'</i>)';
	$path_liste='genrimages/embroider_annotations/'.$ar_infos['nid'];
	$path_del='genrimages/delete_liste_notes/'.$ar_infos['nid'];
	$str_listes.='<div class="listes_item_link">'.l(t($str_title), $path_liste, array('html' => TRUE, )).' ('.l('effacer', $path_del, array('html' => TRUE, )).')</div>';
      }
    }

    if(isset($n->field_visuels['und'])){
      $str_visuels='<hr /><h2>Créer des notes et questionnaires pour la fiche</h2>';
      $visuels=array();
      foreach($n->field_visuels['und'] as $vis){
	$visuels[]=$vis['target_id'];
	/* print_r($vis['target_id']); */
	$vsl=node_load($vis['target_id']);
	/* print_r($vsl->field_visuel['und']); */
	$path_embroider='genrimages/create_liste_notes/'.$n->nid.'/'.$vis['target_id'];
	$im_resized = theme(
			    'image_style',
			    array(
				  'path' => $vsl->field_visuel['und'][0]['uri'],
				  'style_name' => 'medium',
				  'alt' => $vsl->field_description_non_voyants['und'][0]['safe_value'],
				  'title' => $vsl->title,
				  )
			    );
	$str_visuels.='<div class="visuels_item"><div class="visuel_item_image">'.$im_resized.'</div>';
	$str_visuels.='<div class="visuels_item_link">'.l(t('Ajouter des notes à ce visuel'), $path_embroider, array('html' => TRUE, )).'</div>';
	$str_visuels.='<div class="visuels_item_link">'.l(t('Ajouter un questionnaire à ce visuel'), $path_embroider.'/q', array('html' => TRUE, )).'</div></div>';

      }
    }else{
      $visuels=array();
    }

    if(isset($n->field_prolongements_fiches['und'])){
      $prolongements_fiches=array();
      foreach($n->field_prolongements_fiches['und'] as $vis){
	$prolongements_fiches[]=$vis['target_id'];
      }
    }else{
      $prolongements_fiches=array();
    }

    if(isset($n->field_prolongements_visuels['und'])){
      $prolongements_visuels=array();
      foreach($n->field_prolongements_visuels['und'] as $vis){
	$prolongements_visuels[]=$vis['target_id'];
      }
    }else{
      $prolongements_visuels=array();
    }

    if(isset($n->field_prolongements_ressources['und'])){
      $prolongements_ressources=array();
      foreach($n->field_prolongements_ressources['und'] as $vis){
	$prolongements_ressources[]=$vis['target_id'];
      }
    }else{
      $prolongements_ressources=array();
    }



    $hidden_val=$n->nid;



  }

  $str_js_selectors='   <script>
                     jQuery(document).ready(function($) {';
  foreach($selectors as $sel){
    $str_js_selectors.=tinymce_init($sel);
}
  $str_js_selectors.= gi_js();
  $str_js_selectors.='});
                         </script>';

  $str_mots='<hr /><h2>Informations de recherche</h2>';

  /* print_r($hidden_val); */
  $form['title_page'] = array(
  			       '#type' => 'markup',
  			       '#markup' => '<h1>'.t('Fiche').'</h1>',
  			       );
  // This is the first form element. It's a textfield with a label, "Name"
  $form['nom'] = array(
		       '#type' => 'textfield',
		       '#default_value'=>$nom,
		       '#title' => t('Nom'),
		       );
  $form['visuels']=array(
  			 '#multiple' => true,
  			 '#type' => 'select',
  			 '#options' => get_visuels(),
  			 '#default_value' => $visuels,
  			 '#title' => t('choisir les visuels'),
  			 );

  $form['methode'] = array(
  			  '#type' => 'textarea',
  			  '#cols' => 40,
  			  '#default_value'=>$methode,
  			  '#title' => t('Méthode'),
  			  '#format' => 'full_html',
  			  );
  $form['analyse'] = array(
  			  '#type' => 'textarea',
  			  '#cols' => 40,
  			  '#default_value'=>$analyse,
  			  '#title' => t('Analyse'),
  			  '#format' => 'full_html',
  			  );
  $form['prolongements'] = array(
  			  '#type' => 'textarea',
  			  '#cols' => 40,
  			  '#default_value'=>$prolongements,
  			  '#title' => t('Prolongements'),
  			  '#format' => 'full_html',
  			  );
  $form['prolongements_fiches']=array(
  			 '#multiple' => true,
  			 '#type' => 'select',
  			 '#options' => get_prolongements('annotation_ensemble'),
  			 '#default_value' => $prolongements_fiches,
  			 '#title' => t('Prolongements: choisir des fiches liées'),
  			 );
  $form['prolongements_visuels']=array(
  			 '#multiple' => true,
  			 '#type' => 'select',
  			 '#options' => get_visuels(),
  			 '#default_value' => $prolongements_visuels,
  			 '#title' => t('Prolongements: suggérer des visuels à annoter'),
  			 );
  $form['prolongements_ressources']=array(
  			 '#multiple' => true,
  			 '#type' => 'select',
  			 '#options' => get_prolongements('ressource'),
  			 '#default_value' => $prolongements_ressources,
  			 '#title' => t('Prolongements: choisir des ressources liées'),
  			 );
  $form['str_mots']=array(
  			     '#type'=>'markup',
  			     '#markup'=>$str_mots
  			     );
  $form['mots_cles'] = array(
  		       '#type' => 'textfield',
  		       '#default_value'=>$mots_cles,
  		       '#title' => t('Mots clés'),
  		       );
  $form['mediums'] = array(
			   /* '#type' => 'textfield', */
			   '#type' => 'hidden',
			   '#default_value'=>$mediums,
			   /* '#title' => t('Mediums'), */
			   );
  $form['niveaux'] = array(
  		       '#type' => 'textfield',
  		       '#default_value'=>$niveaux,
  		       '#title' => t('Niveaux'),
  		       );
  $form['hidden_id'] = array(
  			     '#type' => 'hidden',
  			     '#default_value' => $hidden_val,
  			     );
  $form['enregistrer'] = array(
  			       '#type' => 'submit',
  			       '#value' => t('Enregistrer'),
  			       );
  $form['str_listes']=array(
  			     '#type'=>'markup',
  			     '#markup'=>$str_listes
  			     );
  $form['str_visuels']=array(
  			     '#type'=>'markup',
  			     '#markup'=>$str_visuels
  			     );
  $form['str_js_selectors']=array(
  			     '#type'=>'markup',
  			     '#markup'=>$str_js_selectors
  			     );
  return $form;
}

function form_annotation_ensemble_edit_submit($form, &$form_state){
  if($form_state['values']['hidden_id']==0){
    /* dpm($form_state['values']); */
    $nid=form_annotation_ensemble_create_node($form_state);
  }else{
    $nid=form_annotation_ensemble_edit_node($form_state);
}
  /* drupal_set_message("Form submitted"); */
  $form_state['redirect'] = 'genrimages/annotation_ensemble_edit/'.$nid;
}


function form_annotation_ensemble_create_node($form_state){
  global $user;
  $node = new stdClass(); // We create a new node object
  $node->type = "annotation_ensemble"; // Or any other content type you want
  node_object_prepare($node); // Set some default values.
  $node->title = $form_state['values']['nom'];
  /* $node->language = "und"; // Or any language code if Locale module is enabled. More on this below  */
  $node->field_analyse['und'][0]['value']= $form_state['values']['analyse'];
  $node->field_methode['und'][0]['value']= $form_state['values']['methode'];
  $node->field_prolongements['und'][0]['value']= $form_state['values']['prolongements'];
  $node->field_mots_cles['und'][0]['value']= $form_state['values']['mots_cles'];
  $node->field_niveaux['und'][0]['value']= $form_state['values']['niveaux'];
  $node->field_mediums['und'][0]['value']= $form_state['values']['mediums'];
  $opts_visuels=array_filter($form_state['values']['visuels']);
  $node->field_visuels['und']=array();
  foreach($opts_visuels as $el){
    $node->field_visuels['und'][] = array('target_id' => $el, 'target_type' => 'node');
  }
  $opts_prolongements_fiches=array_filter($form_state['values']['prolongements_fiches']);
  $node->field_prolongements_fiches['und']=array();
  foreach($opts_prolongements_fiches as $el){
    $node->field_prolongements_fiches['und'][] = array('target_id' => $el, 'target_type' => 'node');
  }
  $opts_prolongements_visuels=array_filter($form_state['values']['prolongements_visuels']);
  $node->field_prolongements_visuels['und']=array();
  foreach($opts_prolongements_visuels as $el){
    $node->field_prolongements_visuels['und'][] = array('target_id' => $el, 'target_type' => 'node');
  }
  $opts_prolongements_ressources=array_filter($form_state['values']['prolongements_ressources']);
  $node->field_prolongements_ressources['und']=array();
  foreach($opts_prolongements_ressources as $el){
    $node->field_prolongements_ressources['und'][] = array('target_id' => $el, 'target_type' => 'node');
  }
  $node->uid = $user->uid;
  // Entity reference field
  $node = node_submit($node); // Prepare node for a submit
  node_save($node); // After this call we'll get a nid 
  /* dpm($node); */
  return $node->nid;
}

function form_annotation_ensemble_edit_node(&$form_state){
  global $user;
  $node = node_load($form_state['values']['hidden_id']);
  dpm(array_filter($form_state['values']['prolongements_fiches']));
  $node->title = $form_state['values']['nom'];
  /* $node->language = "und"; // Or any language code if Locale module is enabled. More on this below  */
  $node->field_analyse['und'][0]['value']= $form_state['values']['analyse'];
  $node->field_methode['und'][0]['value']= $form_state['values']['methode'];
  /* $node->field_methode['und'][0]['value']= $form_state['values']['methode']; */
  $node->field_prolongements['und'][0]['value']= $form_state['values']['prolongements'];
  $node->field_mots_cles['und'][0]['value']= $form_state['values']['mots_cles'];
  $node->field_niveaux['und'][0]['value']= $form_state['values']['niveaux'];
  $node->field_mediums['und'][0]['value']= $form_state['values']['mediums'];
  $opts_visuels=array_filter($form_state['values']['visuels']);
  $node->field_visuels['und']=array();
  foreach($opts_visuels as $el){
    $node->field_visuels['und'][] = array('target_id' => $el, 'target_type' => 'node');
  }
  $opts_prolongements_fiches=array_filter($form_state['values']['prolongements_fiches']);
  $node->field_prolongements_fiches['und']=array();
  foreach($opts_prolongements_fiches as $el){
    $node->field_prolongements_fiches['und'][] = array('target_id' => $el, 'target_type' => 'node');
  }
  $opts_prolongements_visuels=array_filter($form_state['values']['prolongements_visuels']);
  $node->field_prolongements_visuels['und']=array();
  foreach($opts_prolongements_visuels as $el){
    $node->field_prolongements_visuels['und'][] = array('target_id' => $el, 'target_type' => 'node');
  }
  $opts_prolongements_ressources=array_filter($form_state['values']['prolongements_ressources']);
  $node->field_prolongements_ressources['und']=array();
  foreach($opts_prolongements_ressources as $el){
    $node->field_prolongements_ressources['und'][] = array('target_id' => $el, 'target_type' => 'node');
  }
  $node->uid = $user->uid;
 /* dpm($node); */
  node_save($node);
  return $node->nid;
}

function get_visuels(){
  $opts=array();
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'document_visuel')
    ->propertyOrderBy('title', 'ASC');
    /* dpm($query); */
    $result = $query->execute();
  $nids = array_keys($result['node']);
  $nodes = node_load_multiple($nids);
  foreach($nodes as $n){
    $opts[$n->nid]=$n->title;
}
  return $opts;
}


function get_prolongements($bundle){
  $opts=array();
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $bundle)
    ->propertyOrderBy('title', 'ASC');
    /* dpm($query); */
    $result = $query->execute();
  $nids = array_keys($result['node']);
  $nodes = node_load_multiple($nids);
  foreach($nodes as $n){
    $opts[$n->nid]=$n->title;
}
  return $opts;

}
?>