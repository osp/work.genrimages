<?php
function form_annotation_expand($form, &$form_state) {
  global $user;
  if ($_SERVER['SERVER_NAME'] === 'localhost') {
    $url = "/genrimages/plateforme/site/?q=genrimages/";
  }else{
    $url = "/plateforme/?q=genrimages/";
  }
  $hidden_val=0;
  $title="";
  $text='';
  $test=arg(2);

  if(is_numeric($test)){
    /* dpm(arg(2)); */
    $id=(int) arg(2);
    $n=node_load($id);
    /* dpm($n); */
    $title=$n->title;
    $text=$n->body['und'][0]['safe_value'];
    $hidden_val=$n->nid;
    $lids=get_liste_notes_from_annotation($id);
    $fpath='genrimages/embroider_annotations/'.$lids[0];
    $link_fiche=l(t('Retour'), $fpath, array('attributes' => array('class' => 'menu_link'),'html'=>TRUE));
    $wrapper=entity_metadata_wrapper('node',$lids[0]);
    /* print_r($wrapper->field_visuel_ref->field_visuel->value()[0]); */
    $img_uri=$wrapper->field_visuel_ref->field_visuel->value()['uri'];
    $img_width=$wrapper->field_visuel_ref->field_visuel->value()['width'];  
    $img_height=$wrapper->field_visuel_ref->field_visuel->value()['height'];
    $img_alt=$wrapper->field_visuel_ref->field_description_non_voyants->value();
    $img_out = theme('image', array('path' => $img_uri, 'width' => $img_width, 'height' => $img_height, 'alt' => t($img_alt), 'attributes' => array('id'=>'toAnnotate')));

    $str_js='		<script language="javascript">
			   jQuery(document).ready(function($) {
       var cookie_config="notes-'.$user->uid.'-'.$lids[0].'";
       console.log(cookie_config);
       if(Cookies.get(cookie_config)){
	 var config_space=Cookies.getJSON(cookie_config);
         console.log(config_space);
         if(config_space.exp_top){
	   $("#panel_right_expand").offset({top:config_space.exp_top,left:config_space.exp_left});

}
       }else{
         var config_space={};
       }
    $( "#panel_right_expand" ).draggable({ 
      cancel: ".form-textarea-wrapper",
      stop: function() {
	  var offset=$( "#panel_right_expand" ).offset();
	  config_space.exp_top=offset.top;
	  config_space.exp_left=offset.left;
	  console.log("Writing "+cookie_config+" value "+config_space.exp_top);
	  Cookies.set(cookie_config,config_space);
      } 
	  });
				$("#toAnnotate").annotateImage({
					editable: true,
					useAjax: false,
					notes: [ { "top": '.$n->field_top['und'][0]['value'].',
							   "left":'.$n->field_left['und'][0]['value'].',
							   "width":'.$n->field_width['und'][0]['value'].',
							   "height":'.$n->field_height['und'][0]['value'].',
							   "text": '.trim(drupal_json_encode($n->body['und'][0]['safe_value'])).',
							   "id": "'.$n->nid.'",
							   "editable": false },
 ]
				});
  tinymce.init({
    selector: "#edit-text",
    theme: "modern",
    height: 400,
    width:430,
    plugins: [
      "advlist autolink link lists charmap print preview hr anchor",
      "searchreplace wordcount visualblocks visualchars fullscreen",
      "save contextmenu paste textcolor"
    ],
    language: "fr_FR",
    remove_linebreaks : true,
    forced_root_block : "",
    removed_menuitems: "newdocument, image",
    statusbar: false,
    content_css: "css/editor.css",
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
     setup: function(editor) {
	 editor.addMenuItem("autre_fragment", {
	     text: "Lien vers un fragment",
	     context: "insert",
	     onclick: function() {
		 edit_fragments(editor,"fragment");
	     }
	 });
     }
  });



function edit_fragments(leditor,rtype){
    var url="'.$url.'edit_fragments/";
    if(rtype=="fragment"){}else{
      url+=rtype;
    }
    console.log(leditor.settings.selector);
    var editor_el= $("label[for="+leditor.settings.id+"]").next();
    //var label_el = $("label[for=\'"+leditor.settings.id+"\']");
    //var editor_el= label_el+" div";
    $("#edit_liens").show();
    $("#edit_liens").draggable({ handle: "#edit_liens_title" });
    var offset=$(editor_el).offset();
    var owidth=$(editor_el).outerWidth();
    $("#edit_liens").offset({ top: offset.top, left: (offset.left+owidth) });
    $("#edit_liens").html("<div class=\'edit_message\'>Chargement ...</div>");
    $.get( url, function( data ) {
	$( "#edit_liens" ).html( data );
        $(".edit_fermer").click(function(){
          $("#res_liens").hide();
        });
    $(".edit_navig_ln_item").each(function(){
      $(this).click(function(){
        var did=$(this).attr("data-id");
        $("#edit_liens").html("<div class=\'edit_message\'>Chargement ...</div>");
       edit_fragments(leditor,did);
      });
    });
	//console.log( "Load was performed." );
    });
}
			});
		</script>';

    $str_del='<span class="gi_del_button"><a href="?q=genrimages/delete_annotation_fragment/'.$id.'">Effacer la note</a></span>';

    /* print_r($hidden_val); */
    /* $form['description'] = array( */
    /* 				 '#type' => 'item', */
    /* 				 '#title' => t('Éditer une note ou question'), */
    /* 				 ); */

  $str_edit_liens='<div id="edit_liens"></div>';

    $form['panel_left']=array(
			      '#type' => 'markup',
			      "#markup"=>'<div id="panel_left"><div>'.$link_fiche.'</div><div class="" style="margin-bottom:40px;">'.$img_out.'</div></div>');

    $form['panel_right']=array(
			       '#type' => 'markup',
			       "#markup"=>'<div id="panel_right_expand" style="float:left;min-width:300px;max-width:600px;padding-left:80px">');

    $form['text'] = array(
			  '#type' => 'textarea',
			  '#default_value'=>$text,
			  '#title' => t('Texte'),
			  );

    $form['hidden_id'] = array(
			       '#type' => 'hidden',
			       '#default_value' => $hidden_val,
			       );
    $form['enregistrer'] = array(
				 '#type' => 'submit',
				 '#value' => t('Enregistrer'),
				 );
    $form['str_del']=array(
			  '#type' => 'markup',
			  "#markup"=>$str_del);
    $form['end_panel_right']=array(
				   '#type' => 'markup',
				   "#markup"=>'</div>');

    $form['str_js']=array(
			  '#type' => 'markup',
			  "#markup"=>$str_js);
    return $form;

  }else{

    drupal_goto('genrimages/intro');
  }
}

function form_annotation_expand_submit($form, &$form_state){
  if($form_state['values']['hidden_id']==0){
    /* dpm($form_state['values']); */
    /* $nid=form_annotation_expand_create_node($form_state); */
    die('No id error');
  }else{
    $nid=form_annotation_expand_edit_node($form_state);
}
  /* drupal_set_message("Form submitted"); */
  $form_state['redirect'] = 'genrimages/annotation_expand/'.$nid;
}


function form_annotation_expand_edit_node(&$form_state){
  global $user;
  $node = node_load($form_state['values']['hidden_id']);
  /* dpm($form_state['values']['name']); */
  $node->title = mb_substr(strip_tags($form_state['values']['text']),0,120);
  /* $node->language = "und"; // Or any language code if Locale module is enabled. More on this below  */
  $node->body['und'][0]['value']= $form_state['values']['text'];
  $node->uid = $user->uid;
 /* dpm($node); */
  node_save($node);
  return $node->nid;
}


?>