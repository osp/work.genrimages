<?php

function form_annotation_test($form, &$form_state) {
  $form = array();
  if(!isset($form_state['values']['hidden_id'])){
    $hidden_val=0;
  }else{
    $hidden_val=$form_state['values']['hidden_id'];
}
  drupal_set_message(t('Hidden id is '.$hidden_val));
  $title="";
  $top="";
  $left="";
  $height="";
  $width="";
  $fname="";
  $test=arg(2);

  $form['description'] = array(
			       '#type' => 'item',
			       '#title' => t('Testing the annotation form'),
			       );

  $form['title'] = array(
			 '#type' => 'textfield',
			 '#default_value'=>$title,
			 '#title' => t('Titre'),
			 );

  $form['top'] = array(
		       '#type' => 'textfield',
		       '#default_value'=>$top,
		       '#title' => t('Top'),
		       );

  $form['left'] = array(
			'#type' => 'textfield',
			'#default_value'=>$left,
			'#title' => t('Left'),
			);

  $form['width'] = array(
			 '#type' => 'textfield',
			 '#default_value'=>$width,
			 '#title' => t('Width'),
			 );
  $form['height'] = array(
			  '#type' => 'textfield',
			  '#default_value'=>$height,
			  '#title' => t('Height'),
			  );
  $form['fname'] = array(
			 '#type' => 'textfield',
			 '#default_value'=>$fname,
			 '#title' => t('Fname'),
			 );
  $form['hidden_id'] = array(
			     '#type' => 'hidden',
			     '#default_value' => $hidden_val,
			     '#attributes' => array(
						    'id' => 'hidden_id',
						    )
			     );

 $form['enregistrer'] = array(
			       '#type' => 'submit',
			       '#value' => t('Enregistrer'),
			      '#ajax' => array(

					       'callback' => 'form_annotation_test_callback',
					       'wrapper' => 'hidden_id',
					       ),
			       );

 $form['message_zone']=array(
			     '#type' => 'markup',
			     '#markup' => '<div id="message_zone">état initial de la zone de message.</div>',
);

  return $form;
}

function form_classe_submit($form, &$form_state){
  $form_state['rebuild'] = true;
}


function form_annotation_test_callback($form, &$form_state) {
  // The form has already been submitted and updated. We can return the replaced
  // item as it is.
  $text=$form_state['values']['title'];
  if($form_state['values']['hidden_id']==0){
    /* dpm($form_state['values']); */
    $nid=form_annotation_test_create_node($form_state);
    $form_state['values']['hidden_id']=$nid;
  }else{
    $nid=form_annotation_test_edit_node($form_state);
    $form_state['values']['hidden_id']=$nid;
    /* $nid=51; */
}

  /* $commands = array(); */
  /* $form['hidden_id']['value']=$nid; */
  /* $commands[] = ajax_command_replace('#hidden_id', drupal_render($form['hidden_id'])); */
  /* $commands[] = ajax_command_html("#description","<b>test</b>"); */
  /* drupal_set_message("NID is ".$nid); */
  /* return array('#type' => 'ajax', '#commands' => $commands); */

  return array(
	       '#type' => 'markup',
	       '#markup' => '<input type="hidden" id="hidden_id" name="hidden_id" value="'.$nid.'" >',
);
}

function form_annotation_test_create_node($form_state){
  global $user;
  $node = new stdClass(); // We create a new node object
  $node->type = "annotation"; // Or any other content type you want
  node_object_prepare($node); // Set some default values.
  $node->title = $form_state['values']['title'];
  /* $node->language = "und"; // Or any language code if Locale module is enabled. More on this below  */
  $node->field_top['und'][0]['value']= $form_state['values']['top'];
  $node->field_left['und'][0]['value']= $form_state['values']['left'];
  $node->field_height['und'][0]['value']= $form_state['values']['height'];
  $node->field_width['und'][0]['value']= $form_state['values']['width'];
  $node->field_fname['und'][0]['value']= $form_state['values']['fname'];
  $node->uid = $user->uid;
  // Entity reference field
  $node = node_submit($node); // Prepare node for a submit
  node_save($node); // After this call we'll get a nid 
  /* dpm($node); */
  return $node->nid;
}

function form_annotation_test_edit_node(&$form_state){
  global $user;
  $node = node_load($form_state['values']['hidden_id']);
  /* dpm($form_state['values']['name']); */
  $node->title = $form_state['values']['title'];
  /* $node->language = "und"; // Or any language code if Locale module is enabled. More on this below  */
  $node->field_top['und'][0]['value']= $form_state['values']['top'];
  $node->field_left['und'][0]['value']= $form_state['values']['left'];
  $node->field_height['und'][0]['value']= $form_state['values']['height'];
  $node->field_width['und'][0]['value']= $form_state['values']['width'];
  $node->field_fname['und'][0]['value']= $form_state['values']['fname'];
  $node->uid = $user->uid;
  // Entity reference field
  $node = node_submit($node); // Prepare node for a submit
  node_save($node); // After this call we'll get a nid 
  /* dpm($node); */
  return $node->nid;
}


?>