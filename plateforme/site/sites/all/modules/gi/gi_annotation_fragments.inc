<?php
function create_annotation_fragment(){
  global $user;
  $config=json_decode($_POST['config']);
  $attach=false;
  if(($config->id=="null") | (preg_match('/^gi_/',$config->id))){
    $node = new stdClass(); // We create a new node object
    $node->type = "annotation"; // Or any other content type you want
    node_object_prepare($node); // Set some default values.
    $attach=true;
  }else{
    $node = node_load($config->id);
  }
  $node->title = mb_substr(strip_tags($config->text),0,120);
  $node->body['und'][0]['value'] = $config->text;
  $node->body['und'][0]['summary'] = text_summary($config->text);
  $node->body['und'][0]['format'] = 'full_html';
  /* $node->language = "und"; // Or any language code if Locale module is enabled. More on this below  */
  $node->field_top['und'][0]['value']= $config->top;
  $node->field_left['und'][0]['value']= $config->left;
  $node->field_height['und'][0]['value']= $config->height;
  $node->field_width['und'][0]['value']= $config->width;
  $node->field_fname['und'][0]['value']= "no name";
  $node->uid = $user->uid;
  $node = node_submit($node); // Prepare node for a submit
  node_save($node); // After this call we'll get a nid
  $config->id=$node->nid;
  if($attach){
    $liste_notes=node_load($config->liste_notes);
    $liste_notes->field_annotations['und'][] = array('target_id' => $config->id, 'target_type' => 'node');
    node_save($liste_notes);
  }
  header('Content-Type: application/json');
  drupal_json_output($config);
  
}

function get_js_annotations(){
  $id=(int) arg(2);
  $ar_annots=array(); 
  $wrapper=entity_metadata_wrapper('node',$id);
  /* var_dump($wrapper->getPropertyInfo()); */
  foreach ($wrapper->field_annotations as $delta => $annot_wrapper) {
    /* dpm($annot_wrapper); */
    /* $term_wrapper may now be accessed as a taxonomy term wrapper. */
    $js_annot=new js_annotation();
    $js_annot->id=$annot_wrapper->nid->value();
    $js_annot->text=strip_tags($annot_wrapper->title->value());
    $js_annot->top=$annot_wrapper->field_top->value();
    $js_annot->left= $annot_wrapper->field_left->value();
    $js_annot->width= $annot_wrapper->field_width->value();
    $js_annot->height= $annot_wrapper->field_height->value();
    $ar_annots[]=$js_annot;
  }
  header('Content-Type: application/json');
  drupal_json_output($ar_annots);

}


function edit_fragments(){
  $id=(int) arg(2);
  $notes=get_all_liste_notes();
  $str_html='<div id="res_liens">';
  $str_html.='<div id="edit_liens_title">Choisir une fiche, puis glisser-déposer une annotation dans l\'éditeur.</div>';
  $str_html.='<div class="edit_navig_ln">';
  foreach($notes as $n){
    $pth='genrimages/get_fragments_from_liste_documents/'.$n['nid'];
    $class_sel='';
    if($n['nid']==$id){
      $class_sel='_on';
}
    $str_html.='<span class="edit_navig_ln_item'.$class_sel.'" data-id="'.$n['nid'].'">'.t($n['title']).'</span> &middot; ';
}
  $str_html.='</div>';
  $str_html.='<div class="edit_results">';
  if ($_SERVER['SERVER_NAME'] === 'localhost') {
    $path = "/var/www/html/genrimages/plateforme/site/sites/default/files/fragments";
    $pub_path="/genrimages/plateforme/site/sites/default/files/fragments";
  }else{
    $path = "/home/genrimagfi/www/plateforme/sites/default/files/fragments";
    $pub_path = "/plateforme/sites/default/files/fragments";
  }
  $ar_annots=array(); 
  $ld=node_load($id);
  $wrapper=entity_metadata_wrapper('node',$id);
  if(isset($ld->field_visuel_ref['und'][0]['target_id'])){
    $dv=node_load($ld->field_visuel_ref['und'][0]['target_id']);
    if(isset($dv->field_visuel['und'])){
    $pub_name=drupal_realpath($dv->field_visuel['und'][0]['uri']);
    foreach ($wrapper->field_annotations as $delta => $annot_wrapper) {
      /* dpm($annot_wrapper); */
      /* $term_wrapper may now be accessed as a taxonomy term wrapper. */
      $coords=array();
      $coords['top']=$annot_wrapper->field_top->value();
      $coords['left']=$annot_wrapper->field_left->value();
      $coords['width']=$annot_wrapper->field_width->value();
      $coords['height']=$annot_wrapper->field_height->value();
      $out_name=$path.'/'.$annot_wrapper->nid->value().'-'.$dv->field_visuel['und'][0]['filename'];
      $out_name_norm=$path.'/normalized/'.$annot_wrapper->nid->value().'-'.$dv->field_visuel['und'][0]['filename'];
      $isim=create_image_fragment($pub_name,$coords,$out_name,$out_name_norm);
      if($isim){
	$img_path=$pub_path.'/normalized/'.$annot_wrapper->nid->value().'-'.$dv->field_visuel['und'][0]['filename'];
	$str_img=theme('image', array('path' => $img_path, 'alt' => check_plain($annot_wrapper->body->value()),'attributes'=>array("style" => "max-height:180px;")));
	$path='genrimages/oriente_annotation/'.$annot_wrapper->nid->value();
	$str_html.='<span class="edit_mixed_pix">'.l($str_img,$path,array('html' => TRUE )).'</span>';
      }
    }}

  }
  $str_html.='</div>';
  $str_html.='<div class="edit_bottom"><span class="edit_fermer">Fermer</span></div>';
  $str_html.='</div>';
  print $str_html;

}

function get_fragments_from_liste_documents(){
  $notes=get_all_liste_notes();
  $str_html='<div class="navig_ln">';
  foreach($notes as $n){
    $pth='genrimages/get_fragments_from_liste_documents/'.$n['nid'];
    $str_html.='<span class="navig_ln_item">'.l(t($n['title']),$pth).'</span> &middot; ';
  }
  $str_html.='</div>';
  $str_html.='<div class="list_fragments">';
  if ($_SERVER['SERVER_NAME'] === 'localhost') {
    $path = "/var/www/html/genrimages/plateforme/site/sites/default/files/fragments";
    $pub_path="/genrimages/plateforme/site/sites/default/files/fragments";
  }else{
    $path = "/home/genrimagfi/www/plateforme/sites/default/files/fragments";
    $pub_path = "/plateforme/sites/default/files/fragments";
  }
  $id=(int) arg(2);
  $ar_annots=array(); 
  $ld=node_load($id);
  $wrapper=entity_metadata_wrapper('node',$id);
  if(isset($ld->field_visuel_ref['und'][0]['target_id'])){
    $dv=node_load($ld->field_visuel_ref['und'][0]['target_id']);
    if(isset($dv->field_visuel['und'])){
      $pub_name=drupal_realpath($dv->field_visuel['und'][0]['uri']);
      foreach ($wrapper->field_annotations as $delta => $annot_wrapper) {
	/* dpm($annot_wrapper); */
	/* $term_wrapper may now be accessed as a taxonomy term wrapper. */
	$coords=array();
	$coords['top']=$annot_wrapper->field_top->value();
	$coords['left']=$annot_wrapper->field_left->value();
	$coords['width']=$annot_wrapper->field_width->value();
	$coords['height']=$annot_wrapper->field_height->value();
	$out_name=$path.'/'.$annot_wrapper->nid->value().'-'.$dv->field_visuel['und'][0]['filename'];
	$out_name_norm=$path.'/normalized/'.$annot_wrapper->nid->value().'-'.$dv->field_visuel['und'][0]['filename'];
	$isim=create_image_fragment($pub_name,$coords,$out_name,$out_name_norm);
	if($isim){
	  $str_html.='<div style="float:left;background-image:url(\''.$pub_path.'/normalized/'.$annot_wrapper->nid->value().'-'.$dv->field_visuel['und'][0]['filename'].'\');background-size:contain;background-repeat:no-repeat;width:200px;background-position:center;height:200px;border:1px solid black;"></div>';
	}
      }}
  }
  $str_html.='</div>';
  return $str_html;

}

function create_image_fragment($pub_name,$coords,$out_name,$out_name_norm){
  $allowed_extensions=array('jpg','jpeg','png', 'gif');
  //check how file name ends and create and save format accordingly
  $ext = strtolower(pathinfo($pub_name, PATHINFO_EXTENSION));
  if(in_array($ext,$allowed_extensions)){
    if($ext=='png'){
      $im = imagecreatefrompng($pub_name);
    }else if($ext=='gif'){
      $im = imagecreatefromgif($pub_name);
    }else{
      $im = imagecreatefromjpeg($pub_name);
    }
    list($width_orig, $height_orig) = getimagesize($pub_name);
    $image_p=imagecreatetruecolor($coords['width'], $coords['height']);
    $norm_width=200;
    if($coords['width']<$norm_width){
      $norm_factor=$norm_width/$coords['width'];
      $norm_height=$coords['height']*$norm_factor;
    }else{
      $norm_factor=$coords['width']/$norm_width;
      $norm_height=$coords['height']/$norm_factor;

    }
    /* print_r($coords); */
    /* print $norm_height."\n"; */
    /* print $norm_width."\n"; */

    $image_norm=imagecreatetruecolor($norm_width,$norm_height);
    imagecopyresampled($image_p, $im, 0, 0,$coords['left'], $coords['top'],$coords['width'], $coords['height'], $coords['width'], $coords['height']);
    imagecopyresampled($image_norm, $image_p, 0, 0,0, 0,$norm_width, $norm_height, $coords['width'], $coords['height']);
    if($ext=='png'){
      imagepng($image_p,$out_name);
      /* print $out_name_norm."\n"; */
      imagepng($image_norm,$out_name_norm);
    }else if($ext=='gif'){
      imagegif($image_p,$out_name);
      /* print $out_name_norm."\n"; */
      imagegif($image_norm,$out_name_norm);
    }else{
      imagejpeg($image_p,$out_name);
      /* print $out_name_norm."\n"; */
      imagejpeg($image_norm,$out_name_norm);
    }
    imagedestroy($im);
    imagedestroy($image_p);
    imagedestroy($image_norm);
    return true;
  }else{
    return false;
  }
}


function order_annotation_fragments(){
  global $user;
  $config=json_decode($_POST['config']);
  $node = node_load($config->liste_notes);
  $node->field_annotations['und'] = array();
  foreach($config->order as $el){
    $node->field_annotations['und'][] = array('target_id' => $el, 'target_type' => 'node');
}
  $node = node_submit($node); // Prepare node for a submit
  node_save($node); // After this call we'll get a nid
  header('Content-Type: application/json');
  drupal_json_output($config);
}

function create_liste_notes(){
  global $user;
  $id_fiche=(int) arg(2);
  $fiche=node_load($id_fiche);
  $id_visuel=(int) arg(3);
  $mode='notes';
  if(arg(4)=='q'){
    $mode='questions';
  }
  $node = new stdClass(); // We create a new node object
  $node->type = "liste_notes"; // Or any other content type you want
  node_object_prepare($node); // Set some default values.
  $listes_notes=get_liste_notes($id_fiche);
  $cnt_q=1;
  $cnt_n=1;
  foreach($listes_notes as $ar_infos){
    if($ar_infos['type']=='questions'){
      $cnt_q++;
    }else{
      $cnt_n++;
}
    }
  if($mode=='questions'){
    $ttitle=mb_substr($fiche->title,0,40).' : questionnaire '.$cnt_q;
  }else{
    $ttitle=mb_substr($fiche->title,0,40).' : notes '.$cnt_n;
}
  $node->title = $ttitle;
  /* $node->language = "und"; // Or any language code if Locale module is enabled. More on this below  */
  $node->field_fiche['und'][0] = array('target_id' => $id_fiche, 'target_type' => 'node');
  $node->field_visuel_ref['und'][0] = array('target_id' => $id_visuel, 'target_type' => 'node');
  $node->field_notes_type['und'][0]['value']= $mode;
  $node->uid = $user->uid;
  // Entity reference field
  $node = node_submit($node); // Prepare node for a submit
  node_save($node); // After this call we'll get a nid 
  /* dpm($node); */
  $redir= 'genrimages/embroider_annotations/'.$node->nid;
  drupal_goto($redir);
}

function embroider_annotations(){
  global $user;
  $id=(int) arg(2);
  $str_annots=''; 
  $wrapper=entity_metadata_wrapper('node',$id);
  /* $fid=$wrapper->fiche->value(); */
  $mode=$wrapper->field_notes_type->value();
  $fid=$wrapper->field_fiche->value()->nid;
  $ftitle=$wrapper->field_fiche->value()->title;
  $fpath='genrimages/annotation_ensemble_edit/'.$fid;
  $link_fiche=l(t('Voir la fiche <i>'.$ftitle.'</i>'), $fpath, array('attributes' => array('class' => 'menu_link'),'html'=>TRUE));
  /* print_r($wrapper->field_visuel_ref->field_visuel->value()[0]); */
  $img_uri=$wrapper->field_visuel_ref->field_visuel->value()['uri'];
  $img_width=$wrapper->field_visuel_ref->field_visuel->value()['width'];  
  $img_height=$wrapper->field_visuel_ref->field_visuel->value()['height'];
  $img_alt=$wrapper->field_visuel_ref->field_description_non_voyants->value();
  /* var_dump($wrapper->field_visuel_ref->field_visuel->value()['uri']); */
  $img_out = theme('image', array('path' => $img_uri, 'width' => $img_width, 'height' => $img_height, 'alt' => t($img_alt), 'attributes' => array('id'=>'toAnnotate')));
  return array("#markup"=>'<div class="menu_env">'.$link_fiche."</div><h1>".$wrapper->title->value().'</h1><input type="hidden" name="liste_notes_id" id="liste_notes_id" value="'.$id.'"/><input type="hidden" name="mode" id="mode" value="'.$mode.'"/><div class="" style="margin-bottom:40px;">'.$img_out.'</div>');
}

function save_frame($upload_dir){
  $img = $_POST['imgBase64'];
  $img = str_replace('data:image/png;base64,', '', $img);
  $img = str_replace(' ', '+', $img);
  $data = base64_decode($img);
  $img_name= $_POST['imgName'];
  $file = $upload_dir."/".$img_name;
  $success = file_put_contents($file, $data);
  return $success;
}

function create_document_visuel(){
  global $user;
  if ($_SERVER['SERVER_NAME'] === 'localhost') {
    $path = "/var/www/html/genrimages/plateforme/site/sites/default/files/frames";
  }else{
    $path = "/home/genrimagfi/www/plateforme/sites/default/files/frames";
  }
  $file_upload=save_frame($path);
  $node = new stdClass(); // We create a new node object
  $node->type = "document_visuel"; // Or any other content type you want
  node_object_prepare($node); // Set some default values.
  $node->title=$_POST['imgTitle'];
  $node->field_reference['und'][0]['value']= $_POST['vidName'];
  $path_file=$path."/".$_POST['imgName'];
  $file_path = drupal_realpath($path_file);
  /* print_r(file_get_mimetype($file_path)); */
  $file = (object) array(
			 'uid' => 1,
			 'uri' => $file_path,
			 'filemime' => file_get_mimetype($file_path),
			 'status' => 1,
			 'display'=>0
			 );
  $file = file_copy($file, 'public://import', FILE_EXISTS_REPLACE);
  $node->field_visuel['und'][0] = (array) $file;
  $node->uid = $user->uid;
  $node = node_submit($node); // Prepare node for a submit
  node_save($node); // After this call we'll get a nid
}

function delete_annotation_fragment(){
  $id=(int) arg(2);
  $ln=get_liste_notes_from_annotation($id);
  $liste_notes=node_load($ln[0]);
  $tmp_annots=array();
  foreach($liste_notes->field_annotations['und'] as $fa){
    if($fa['target_id']==$id){
      //skip
    }else{
      $tmp_annots[]=$fa;
}
}
  $liste_notes->field_annotations['und']=$tmp_annots;
  /* var_dump($liste_notes); */
  node_save($liste_notes);
  node_delete($id);
  $redir= 'genrimages/embroider_annotations/'.$ln[0];
  drupal_goto($redir);
}


function delete_liste_notes(){
  $id=(int) arg(2);
  $ln=node_load($id);
  /* var_dump($ln->field_fiche); */
  $fiche_id=$ln->field_fiche['und'][0]['target_id'];
  node_delete($id);
  $redir= 'genrimages/annotation_ensemble_edit/'.$fiche_id;
  drupal_goto($redir);
}

function edit_liens(){
  $type=check_plain(arg(2));
  $str_html='<div id="res_liens">';
  if($type=='fiche'){
    $str_html.='<div id="edit_liens_title">Pour choisir une fiche, glisser-déposer un lien dans l\'éditeur.</div>';
    $ar_liens=get_all_fiches();
    $str_html.='<div class="edit_results">';
    foreach($ar_liens as $res){
      $path='genrimages/oriente_fiche/'.$res['nid'];
      $str_html.='<span>'.l(t($res['title']),$path).'</span> <br />';
    }
    $str_html.='</div>';
  }else{
    $str_html.='<div id="edit_liens_title">Pour choisir une ressource, glisser-déposer une image dans l\'éditeur.</div>';
    $ar_liens=get_all_ressources();
    $str_html.='<div class="edit_results">';
    foreach($ar_liens as $res){
      $path='genrimages/oriente_ressource/'.$res['nid'];
      if($res['thumb']){
	$str_html.='<span class="edit_mixed_pix">'.l(t($res['thumb']),$path, array('html' => TRUE )).'</span> ';

}else{
      $str_html.='<span class="edit_mixed">'.l(t($res['title']),$path).'</span> ';
      }}
    $str_html.='<br class="clear" /></div>';
  }

  $str_html.='<div class="edit_bottom"><span class="edit_fermer">Fermer</span></div>';
  $str_html.='</div>';
  print $str_html;
}

function list_videos(){
  $str_videos='<div class="video_list">';
  $d = array_values(array_diff(scandir("/var/www/html/genrimages/echange/Videos/"), array('..', '.')));
  foreach($d as $v){
    $str_videos.='<div class=""><a href="?q=genrimages/create_sequence">'.$v.'</a></div>';
}
  $str_videos.="</div>";
  return $str_videos;
}

function get_all_ressources(){
  $listes=array();
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'ressource')
    ->propertyOrderBy('title', 'ASC');
  /* dpm($query); */
  $result = $query->execute();
  $nids = array_keys($result['node']);
  $nodes = node_load_multiple($nids);
  foreach($nodes as $n){
    $vis=null;
    if(isset($n->field_visuel_ref['und'][0]['target_id'])){
      $dv=node_load($n->field_visuel_ref['und'][0]['target_id']);
      $vis = theme(
			  'image_style',
			  array(
				'path' => $dv->field_visuel['und'][0]['uri'],
				'style_name' => 'insert',
				'alt' => $dv->field_description_non_voyants['und'][0]['safe_value'],
				'title' => $dv->title,
				)
			  );
      /* print_r($vis); */
    }
    $listes[]=array('nid'=>$n->nid,'title'=>$n->title,"thumb"=>$vis);

  }
  /* print_r($listes); */
  return $listes;
}

?>