<?php
//Bootstrap start
define('DRUPAL_ROOT', '/var/www/html/genrimages/plateforme/site');
$_SERVER['REMOTE_ADDR'] = "localhost"; // Necessary if running from command line
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
include_once("includes.php");
$f_name="../annotation/data-sophie-17-02-2017/data/1485964326.ser";
$str_ser=file_get_contents($f_name);
$fiche=unserialize($str_ser);
/* var_dump($fiche); */
$node = new stdClass(); // We create a new node object
$node->type = "annotation_ensemble"; // Or any other content type you want
node_object_prepare($node); // Set some default values.
$node->title = $fiche->get_title();
$node->field_descriptif['und'][0]['value'] = $fiche->get_descriptif();
$node->field_methode['und'][0]['value'] = $fiche->get_method();
$node->field_prolongements['und'][0]['value'] = $fiche->get_prolongements();
$node->field_conclusion['und'][0]['value'] = $fiche->get_conclusion();
$node->field_niveaux['und'][0]['safe_value']=$fiche->get_niveaux();
$node->field_mediums['und'][0]['safe_value']=$fiche->get_mediums();
$node->field_mots_cles['und'][0]['safe_value']=$fiche->get_mots_cles();
$node->language = LANGUAGE_NONE; // Or any language code if Locale module is enabled. More on this below
$node = node_submit($node); // Prepare node for a submit
/* print_r($node); */
node_save($node); // After this call we'll get a nid
/* system("firefox http://localhost/genrimages/plateforme/site/?q=genrimages/annotation_ensemble_edit/".$node->nid); */
?>