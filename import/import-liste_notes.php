<?php
//Bootstrap start
define('DRUPAL_ROOT', '/var/www/html/genrimages/plateforme/site');
$_SERVER['REMOTE_ADDR'] = "localhost"; // Necessary if running from command line
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
include_once("includes.php");
$f_name="../annotation/data-sophie-17-02-2017/data/1485964326.json";
$str_ser=file_get_contents($f_name);
$ar_conf=unserialize($str_ser);
$ar=json_decode($ar_conf["config"]);
$js_name=$ar[0]->fname;
$attach_id=174;
foreach($ar as $annot){
  $node = new stdClass(); // We create a new node object
  $node->type = "annotation"; // Or any other content type you want
  node_object_prepare($node); // Set some default values.
  $node->title = mb_substr(strip_tags($annot->text),0,120);
  $node->body['und'][0]['value'] = $annot->text;
  $node->body['und'][0]['summary'] = text_summary($annot->text);
  $node->body['und'][0]['format'] = 'full_html';
  $node->language = LANGUAGE_NONE; // Or any language code if Locale module is enabled. More on this below
  $node->field_top['und'][0]['value']= $annot->top;
  $node->field_left['und'][0]['value']= $annot->left;
  $node->field_height['und'][0]['value']= $annot->height;
  $node->field_width['und'][0]['value']= $annot->width;
  $node->field_fname['und'][0]['value']= $annot->fname;
  $node->uid = $user->uid;
  $node = node_submit($node); // Prepare node for a submit
  node_save($node); // After this call we'll get a nid
  $annot->id=$node->nid;
  $liste_notes=node_load($attach_id);
  print_r( $liste_notes);
  $liste_notes->field_annotations['und'][] = array('target_id' => $annot->id, 'target_type' => 'node');
  node_save($liste_notes);
  /* var_dump($node); */
}
  system("firefox http://localhost/genrimages/plateforme/site/?q=genrimages/embroider_annotations/".$attach_id);
?>