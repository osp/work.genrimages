<?php
//Bootstrap start
define('DRUPAL_ROOT', '/var/www/html/genrimages/plateforme/site');
$_SERVER['REMOTE_ADDR'] = "localhost"; // Necessary if running from command line
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
include_once("includes.php");
$data_dir="../annotation/data-sophie-23-02-2017/";
$d = array_values(array_diff(scandir($data_dir), array('..', '.')));
foreach($d as $f){
  if(preg_match("/\.ser$/",$f)){
    $f_name=$data_dir.$f;
    $str_ser=file_get_contents($f_name);
    print $f_name."\n";
    $fiche=unserialize($str_ser);
    /* var_dump($fiche); */

    //create a document visuel
    $img_path='images/'.$fiche->get_url();
    $nodev = new stdClass(); // We create a new nodev object
    $nodev->type = "document_visuel"; // Or any other content type you want
    node_object_prepare($nodev); // Set some default values.
    $nodev->title = "Image pour la fiche ".$fiche->get_title();
    $nodev->field_description_non_voyants['und'][0]['value'] = $fiche->get_url_descriptor();
    $file_path = drupal_realpath($img_path);
    $file = (object) array(
			   'uid' => 1,
			   'uri' => $file_path,
			   'filemime' => file_get_mimetype($file_path),
			   'status' => 1,
			   'display'=>0
			   );
    // You can specify a subdirectory, e.g. public://foo/
    /* print_r($file); */
    $file = file_copy($file, 'public://', FILE_EXISTS_REPLACE);
    /* print_r($file); */
    $nodev->field_visuel[LANGUAGE_NONE][0] = (array) $file;
    $nodev->language = LANGUAGE_NONE; // Or any language code if Locale module is enabled. More on this below
    $nodev = node_submit($nodev); // Prepare node for a submit
    node_save($nodev); // After this call we'll get a nid

    //create a fiche
    $fnode = new stdClass(); // We create a new node object
    $fnode->type = "annotation_ensemble"; // Or any other content type you want
    node_object_prepare($fnode); // Set some default values.
    $fnode->title = $fiche->get_title();
    print $fiche->get_title()."\n";
    $fnode->field_analyse['und'][0]['value'] = $fiche->get_descriptif().'<br /><br /><b>Conclusion</b><br /><br />'.$fiche->get_conclusion();
    $fnode->field_methode['und'][0]['value'] = $fiche->get_method();
    $fnode->field_prolongements['und'][0]['value'] = $fiche->get_prolongements();
    $fnode->field_niveaux['und'][0]['safe_value']=$fiche->get_niveaux();
    $fnode->field_mediums['und'][0]['safe_value']=$fiche->get_mediums();
    $fnode->field_mots_cles['und'][0]['safe_value']=$fiche->get_mots_cles();
    $fnode->field_visuels['und'][0]['target_id']=$nodev->nid;
    $fnode->language = LANGUAGE_NONE; // Or any language code if Locale module is enabled. More on this below
    $fnode = node_submit($fnode); // Prepare node for a submit
    /* print_r($fnode); */
    node_save($fnode); // After this call we'll get a nid

    //create a liste_notes to hold the annotations
    $lnode = new stdClass(); // We create a new node object
    $lnode->type = "liste_notes"; // Or any other content type you want
    node_object_prepare($lnode); // Set some default values.
    $lnode->title = "Notes pour ".$fnode->title;
    $lnode->language = LANGUAGE_NONE; // Or any language code if Locale module is enabled. More on this below
    $lnode->field_fiche['und'][0] = array('target_id' => $fnode->nid, 'target_type' => 'node');
    $lnode->field_visuel_ref['und'][0] = array('target_id' => $nodev->nid, 'target_type' => 'node');
    $lnode->field_notes_type['und'][0]['value']= "notes";
    $lnode->uid = 1;
    $lnode = node_submit($lnode); // Prepare node for a submit
    node_save($lnode); // After this call we'll get a nid 
    $attach_id=$lnode->nid;
    $js_name=preg_replace('/\.ser$/','.json',$f_name);
    $str_ser=file_get_contents($js_name);
    $ar_conf=unserialize($str_ser);
    $ar=json_decode($ar_conf["config"]);
    foreach($ar as $annot){
      $anode = new stdClass(); // We create a new node object
      $anode->type = "annotation"; // Or any other content type you want
      node_object_prepare($anode); // Set some default values.
      $anode->title = mb_substr(strip_tags($annot->text),0,120);
      $anode->body['und'][0]['value'] = $annot->text;
      $anode->body['und'][0]['summary'] = text_summary($annot->text);
      $anode->body['und'][0]['format'] = 'full_html';
      $anode->language = LANGUAGE_NONE; // Or any language code if Locale module is enabled. More on this below
      $anode->field_top['und'][0]['value']= $annot->top;
      $anode->field_left['und'][0]['value']= $annot->left;
      $anode->field_height['und'][0]['value']= $annot->height;
      $anode->field_width['und'][0]['value']= $annot->width;
      $anode->field_fname['und'][0]['value']= $annot->fname;
      $anode->uid = $user->uid;
      $anode = node_submit($anode); // Prepare node for a submit
      node_save($anode); // After this call we'll get a nid
      $annot->id=$anode->nid;
      $liste_notes=node_load($attach_id);
      /* print_r( $liste_notes); */
      $liste_notes->field_annotations['und'][] = array('target_id' => $annot->id, 'target_type' => 'node');
      node_save($liste_notes);
      /* var_dump($anode); */
    }
  }}
  /* system("firefox http://localhost/genrimages/plateforme/site/?q=genrimages/embroider_annotations/".$attach_id); */
?>