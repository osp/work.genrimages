<http://www.genrimages.org/>

Le site Genrimages met à disposition de la communauté éducative des vidéos et images analysées, des ressources et un outil d’annotation d’images fixes et animées pour conduire des séances de sensibilisation qui croisent éducation à l’image et éducation à l’égalité femme-homme.